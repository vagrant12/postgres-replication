#!/bin/bash
# Prepare standby server

#---------- [ Variables ] ----------

ARCHIVE_IP=192.168.0.220
PRIMARY_IP=192.168.0.221
PRIMARY_PORT=5432
STANDBY_IP=192.168.0.222
REPL_USER=replica
REPL_USER_PASS=replica_pass
POSTGRESQL_VERSION=14
PGCONFIG=/etc/postgresql/$POSTGRESQL_VERSION/main
PGDATA=/var/lib/postgresql/$POSTGRESQL_VERSION/main
POSTGRES_HOME_DIR=/var/lib/postgresql
WAL_ARCHIVE_DIR=/data/pg_wal_archive

#---------- [ General ] ----------

# Install additional software
apt update && apt install -y sudo vim mc tree gnupg2

# Create the file repository configuration:
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# Import the repository signing key:
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Update the package lists:
sudo apt update

# Install the latest version of PostgreSQL.
sudo apt install -y postgresql-$POSTGRESQL_VERSION

#---------- [ Main ] ----------

# --- [ Configure NFS client ] ---

sudo mkdir -p $WAL_ARCHIVE_DIR
sudo apt install -y nfs-common
sudo echo "$ARCHIVE_IP:$WAL_ARCHIVE_DIR  $WAL_ARCHIVE_DIR    nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800    0   0" >> /etc/fstab
sudo mount $ARCHIVE_IP:$WAL_ARCHIVE_DIR $WAL_ARCHIVE_DIR

# --- [ Configure PostgreSQL ] ---

# Modify 'listen_address' to allow postgresql listen specific ip address
sed -i "s/#listen_addresses = 'localhost'/listen_addresses = \'$STANDBY_IP\'/g" $PGCONFIG/postgresql.conf
systemctl restart postgresql

# Configure WAL restore
sed -i "s^#restore_command = ''^restore_command = \'cp $WAL_ARCHIVE_DIR/\%f \%p\'^g" $PGCONFIG/postgresql.conf
systemctl restart postgresql

# Create .pgpass file and modify permissions on it
echo "$PRIMARY_IP:$PRIMARY_PORT:replication:$REPL_USER:$REPL_USER_PASS" > $POSTGRES_HOME_DIR/.pgpass
chown postgres:postgres $POSTGRES_HOME_DIR/.pgpass
chmod 0600 $POSTGRES_HOME_DIR/.pgpass

# Clear out PostgreSQL data dir
su - postgres -c "rm -r $PGDATA/*"

# Perform a physical backup of the primary server's data files and make local machine standby server
su - postgres -c "pg_basebackup -h $PRIMARY_IP -p $PRIMARY_PORT -U $REPL_USER -D $PGDATA -Fp -Xs -R"
systemctl restart postgresql