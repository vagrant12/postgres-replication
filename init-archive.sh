#!/bin/bash
# Prepare archive server

#---------- [ Variables ] ----------

ARCHIVE_IP=192.168.0.220
PRIMARY_IP=192.168.0.221
PRIMARY_PORT=5432
STANDBY_IP=192.168.0.222
REPL_USER=replica
REPL_USER_PASS=replica_pass
POSTGRESQL_VERSION=14
PGCONFIG=/etc/postgresql/$POSTGRESQL_VERSION/main
PGDATA=/var/lib/postgresql/$POSTGRESQL_VERSION/main
POSTGRES_HOME_DIR=/var/lib/postgresql
WAL_ARCHIVE_DIR=/data/pg_wal_archive

#---------- [ Main ] ----------

# --- [ Configure NFS server ] ---

apt update && apt install -y sudo vim mc tree gnupg2
sudo apt install -y nfs-kernel-server
sudo systemctl enable nfs-kernel-server --now
sudo mkdir -p $WAL_ARCHIVE_DIR
sudo chown nobody:nogroup $WAL_ARCHIVE_DIR
sudo chmod 0777 $WAL_ARCHIVE_DIR
sudo echo "$WAL_ARCHIVE_DIR $PRIMARY_IP(rw,sync,no_subtree_check)" >> /etc/exports
sudo echo "$WAL_ARCHIVE_DIR $STANDBY_IP(rw,sync,no_subtree_check)" >> /etc/exports
sudo systemctl restart nfs-kernel-server