#!/bin/bash
# Prepare primary server

#---------- [ Variables ] ----------

ARCHIVE_IP=192.168.0.220
PRIMARY_IP=192.168.0.221
PRIMARY_PORT=5432
STANDBY_IP=192.168.0.222
REPL_USER=replica
REPL_USER_PASS=replica_pass
POSTGRESQL_VERSION=14
PGCONFIG=/etc/postgresql/$POSTGRESQL_VERSION/main
PGDATA=/var/lib/postgresql/$POSTGRESQL_VERSION/main
POSTGRES_HOME_DIR=/var/lib/postgresql
WAL_ARCHIVE_DIR=/data/pg_wal_archive

#---------- [ General ] ----------

# Install additional software
apt update && apt install -y sudo vim mc tree gnupg2

# Create the file repository configuration:
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# Import the repository signing key:
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Update the package lists:
sudo apt update

# Install the latest version of PostgreSQL.
sudo apt install -y postgresql-$POSTGRESQL_VERSION

#---------- [ Main ] ----------

# --- [ Configure NFS client ] ---

sudo mkdir -p $WAL_ARCHIVE_DIR
sudo apt install -y nfs-common
sudo echo "$ARCHIVE_IP:$WAL_ARCHIVE_DIR  $WAL_ARCHIVE_DIR    nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800    0   0" >> /etc/fstab
sudo mount $ARCHIVE_IP:$WAL_ARCHIVE_DIR $WAL_ARCHIVE_DIR

# --- [ Configure PostgreSQL ] ---

# Modify 'listen_address' to allow postgresql listen specific ip address
sed -i "s/#listen_addresses = 'localhost'/listen_addresses = \'$PRIMARY_IP\'/g" $PGCONFIG/postgresql.conf
systemctl restart postgresql

# Create role with replication permissions
su - postgres -c "psql -c \"CREATE ROLE $REPL_USER WITH REPLICATION PASSWORD '$REPL_USER_PASS' LOGIN;\""

# Allow external connections to replication base
echo "host  replication $REPL_USER  $STANDBY_IP/32  md5" >> $PGCONFIG/pg_hba.conf
systemctl reload postgresql

# Configure WAL archivation
sed -i "s/#wal_level = replica/wal_level = replica/g" $PGCONFIG/postgresql.conf
sed -i "s/#archive_mode = off/archive_mode = on/g" $PGCONFIG/postgresql.conf
sed -i "s^#archive_command = ''^archive_command = \'test ! -f $WAL_ARCHIVE_DIR/\%f \&\& cp \%p $WAL_ARCHIVE_DIR/\%f\'^g" $PGCONFIG/postgresql.conf
systemctl restart postgresql